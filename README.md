# Chromium Repo for PPC64LE (Testing)

This is a subproject to host metadata for a `dnf` repo for Chromium on PPC64LE.

This repo is automatically populated by CI builds.

